<?php
	// require the base Mage utility
	require 'app/Mage.php';
	//Mage::app();

	// open the coupon-containing file   
	//$fhandler = fopen('coupons.csv', 'r');
	// flip keys and values, set to array
	//$cols = array_flip(fgetcsv($fhandler));

	// while there is output from the csv
	//while($data = fgetcsv($fhandler)){
	//	echo 'Looping through csv @'.$data[$cols['Code']];
	//}

// test creation of coupon
newCoupon('Ktest', 'Test description.', 'KDTEST', '2014-15-05', '2014-20-05', 'by_percent', '25.0000');

	/**
	 * create a new coupon
	 * @param  [string] $name 	coupon's name
	 * @param  [string] $desc 	description of the coupon
	 * @param  [string] $code 	magento coupon code, pref = alphabetical
	 * @param  [string] $from 	the 'from' or start date of the coupon [yyyy-mm-dd]
	 * @param  [string] $to   	the 'to' or end date of the coupon [yyyy-mm-dd]
	 * @param  [string] $type 	the discount type, percentage, dollar value, etc
	 * @param  [string] $amount the amount of the discount [25.0000], [3.0000]
	 * @return 
	 */
	function newCoupon($name, $desc, $code, $from, $to, $type, $amount){
		// instantiate a coupon model
		$coupon = Mage::getModel('salesrule/rule');

		// base input attributes
		$coupon->setName($name);
		$coupon->setDescription($desc);
		$coupon->setCouponCode($code);
		$coupon->setFromDate($from);
		$coupon->setToDate($to);
		$coupon->setSimpleAction($type);
		$coupon->setDiscountAmount($amount);

		// uses/active
		$coupon->setUsesPerCustomer('100');
		$coupon->setIsActive('1');

		// serializing
		$coupon->setConditionsSerialized('a:6:{s:4:"type";s:32:"salesrule/rule_condition_combine";s:9:"attribute";N;s:8:"operator";N;s:5:"value";s:1:"1";s:18:"is_value_processed";N;s:10:"aggregator";s:3:"all";}');
		$coupon->setActionsSerialized('a:6:{s:4:"type";s:40:"salesrule/rule_condition_product_combine";s:9:"attribute";N;s:8:"operator";N;s:5:"value";s:1:"1";s:18:"is_value_processed";N;s:10:"aggregator";s:3:"all";}');

		// processing/advanced
		$coupon->setStopRulesProcessing('0');
		$coupon->setIsAdvanced('1');

		// applicible products/sort order
		$coupon->setProductIds(NULL);
		$coupon->setSortOrder('0');

		// discounting step (Buy X)/ quantity
		$coupon->setDiscountStep('0');
		$coupon->setDiscountQty(NULL);

		// shipping
		$coupon->setSimpleFreeShipping('0');
		$coupon->setApplyToShipping('0');

		// uses/rss visiblity
		$coupon->setTimesUsed('1');
		$coupon->setIsRss('0');

		// coupon scope
		$coupon->setCouponType('2');
		$coupon->setUseAutoGeneration('0');
		$coupon->setUsesPerCoupon('1000');
		$coupon->setCustomerGroupIds(array('1',));
		$coupon->setWebsiteIds(array('1',));		

		// save the coupon
		$coupon->save();
		//$coupon->setType(Mage_SalesRule_Helper_Coupon::COUPON_TYPE_SPECIFIC_AUTOGENERAT‌​ED)->save();
		echo "Completed. Check the coupons page.";
	}

?>